# coding: utf-8
"""."""
import os
from datetime import datetime
import webbrowser

from atlassian import Confluence
from tabulate import tabulate

from .logging import LOG
from .io import load_stats


def update_confluence(filename):
    """."""
    confluence = Confluence(
        url="https://confluence.skatelescope.org/",
        username=os.environ.get("CONFLUENCE_USERNAME"),
        password=os.environ.get("CONFLUENCE_PASSWORD"),
    )
    space = "TS"
    title = "Python API Test 2"

    page_exists = confluence.page_exists(space, title)
    LOG.debug("Page exists? %s", page_exists)

    if not page_exists:
        LOG.info("Creating page ...")
        status = confluence.create_page(space=space, title=title, body="<p></p>")
        LOG.info("status = %s", status)

    page_id = confluence.get_page_id(space, title)
    page = confluence.get_page_by_id(page_id, expand="body.storage")
    LOG.debug("Existing page (id = %s):", page_id)
    LOG.debug("\n%s", page.get("body").get("storage").get("value"))

    stats = load_stats(filename)
    # print(json.dumps(stats, indent=2))

    body = "<p>This is a test page created using the Python API</p>"
    body += "<br />"
    for group in stats:
        # LOG.info("Group: %s", group)
        headers, data = stats_to_table_data(stats[group])
        table = tabulate(data, headers, tablefmt="html")
        # if group != "None":
        #     body += "<h2>{group}</h2>"
        body += table
        # render_html(table)
        # print(table)

    body += "<br />"
    LOG.debug("%s", page_id)
    LOG.debug("%s", title)
    LOG.debug("%s", body)
    LOG.info("Updating: %s", page["_links"]["base"] + page["_links"]["webui"])
    confluence.update_page(page_id, title, body=body)


def stats_to_table_data(stats):
    """."""
    table_headers = ["Name", "Last Update", "Build", "Documentation"]
    table_data = []
    for repo in stats:
        repo_data = []

        # Name
        # print(json.dumps(repo, indent=2))
        name = f'<a href="{repo["url"]}">{repo["name"]}</a>'
        repo_data.append(name)

        # Last Update
        last_update = datetime.strptime(
            repo["last_activity_at"], "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        repo_data.append(f'{last_update.strftime("%Y-%m-%d %H:%M:%S")}')

        # Build status badge
        pipeline_status = ""
        url = repo["badges"]["pipeline"]["url"]
        image = repo["badges"]["pipeline"]["image"]
        # pipeline_status = f'<a href="{url}"><img src="{image}"></a>'
        pipeline_status = (
            f"<p>"
            f'<a href="{url}">'
            f'<ac:image><ri:url ri:value="{image}" /></ac:image></a>'
            f"</p>"
        )
        url = repo["badges"]["build_last_date"]["url"]
        image = repo["badges"]["build_last_date"]["image"]
        pipeline_status += (
            f"<p>"
            f'<a href="{url}">'
            f'<ac:image><ri:url ri:value="{image}" /></ac:image></a>'
            f"</p>"
        )
        repo_data.append(pipeline_status)

        # Documentation
        url = repo["badges"]["Documentation"]["url"]
        image = repo["badges"]["Documentation"]["image"]
        doc = (
            f"<p>"
            f'<a href="{url}">'
            f'<ac:image><ri:url ri:value="{image}" /></ac:image></a>'
            f"</p>"
        )
        repo_data.append(doc)

        # Add the repo to the table
        table_data.append(repo_data)

    return table_headers, table_data


def render_html(html_str):
    """."""
    html_file = "table.html"
    with open(html_file, "w") as file:
        file.write(html_str)
    webbrowser.open_new_tab(html_file)
