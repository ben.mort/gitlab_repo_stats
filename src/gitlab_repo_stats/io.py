# coding: utf-8
"""."""
from .logging import LOG

from ruamel import yaml


def write_stats(stats, filename, append, group):
    """."""
    LOG.info("File:   %s", filename)
    LOG.info("Group:  %s", group)
    LOG.info("Append: %s", append)

    if append:
        _stats = load_stats(filename)
        if group not in _stats.keys():
            _stats[group] = []
        _stats[group].append(stats)
    else:
        _stats = {group: [stats]}

    with open(filename, "w") as outfile:
        yaml.dump(_stats, outfile, Dumper=yaml.RoundTripDumper)


def load_stats(filename):
    """."""
    with open(filename, "r") as file:
        stats = yaml.safe_load(file)
    return stats
