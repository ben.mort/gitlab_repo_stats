# coding: utf-8
"""repo-stats.

Command line utility to get and update repo stats

Usage:
    repo-stats get [-f FILE] [-v] [-a] [-g GROUP] REPO...
    repo-stats write [-f FILE] [-v]

Options:
    -f FILE         Stats filename [default: stats.yml].
    -a              Append to the stats file.
    -g GROUP        Custom group [default: None].
    -h, --help      Show this help.
    -v              Verbose.
    --version       Show the version.

Arguments:
    FILE            File to write to.
    GROUP           Custom group.
    REPO            Repository stub.

"""
from docopt import docopt

# from gitlab.v4.objects import badges
from .logging import init_logger, LOG
from .write import update_confluence
from .io import write_stats
from .get import get_stats


__version__ = "0.0.1"


def process_commands(args):
    """Process commands.

    Args:
        args(dict): Dictionary of command line arguments.
    """
    # LOG.debug("%s", args)
    if args["get"]:
        for repo in args["REPO"]:
            stats = get_stats(repo)
            write_stats(
                stats=stats, filename=args["-f"], group=args["-g"], append=args["-a"]
            )
    if args["write"]:
        update_confluence(args["-f"])


def main():
    """Script main."""
    args = docopt(__doc__, version=__version__)
    init_logger(level="DEBUG" if args["-v"] else "INFO")
    try:
        process_commands(args)
    except (ValueError, RuntimeError) as error:
        print(error)


if __name__ == "__main__":
    main()
